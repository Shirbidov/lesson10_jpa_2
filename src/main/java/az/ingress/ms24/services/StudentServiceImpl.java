package az.ingress.ms24.services;

import az.ingress.ms24.domain.StudentEntity;
import az.ingress.ms24.dto.StudentDto;
import az.ingress.ms24.dto.StudentRequestDto;
import az.ingress.ms24.exception.NotFoundException;
import az.ingress.ms24.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public StudentDto create(StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity.
                        builder()
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);
        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .build();
    }

    @Override
    public StudentDto update(Long id, StudentRequestDto requestDto) {
        StudentEntity student =
                StudentEntity.
                        builder()
                        .id(id)
                        .firstName(requestDto.getFirstName())
                        .lastName(requestDto.getLastName())
                        .age(requestDto.getAge())
                        .studentNumber(requestDto.getStudentNumber())
                        .build();
        final StudentEntity studentEntitySaved = studentRepository.save(student);
        return StudentDto
                .builder()
                .id(studentEntitySaved.getId())
                .studentNumber(studentEntitySaved.getStudentNumber())
                .firstName(studentEntitySaved.getFirstName())
                .lastName(studentEntitySaved.getLastName())
                .age(studentEntitySaved.getAge())
                .build();
    }

    @Override
    public StudentDto get(Long id) {
        final StudentEntity studentEntity = studentRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return StudentDto
                .builder()
                .id(studentEntity.getId())
                .studentNumber(studentEntity.getStudentNumber())
                .firstName(studentEntity.getFirstName())
                .lastName(studentEntity.getLastName())
                .age(studentEntity.getAge())
                .build();
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Page<StudentDto> list(@PageableDefault(size = 5, page = 1)
                                 Pageable pageable) {
        return studentRepository.findAll(pageable)
                .map(studentEntity -> StudentDto
                        .builder()
                        .id(studentEntity.getId())
                        .studentNumber(studentEntity.getStudentNumber())
                        .firstName(studentEntity.getFirstName())
                        .lastName(studentEntity.getLastName())
                        .age(studentEntity.getAge())
                        .build());
    }
}
