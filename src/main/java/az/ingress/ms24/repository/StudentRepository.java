package az.ingress.ms24.repository;

import az.ingress.ms24.domain.StudentEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> , JpaSpecificationExecutor<StudentEntity> {

//    void findAllByFirstnameDistinct();

//    List<StudentEntity> findAllByIdGreaterThan(Long id);
//    List<StudentEntity> findAllByGreaterThanAndFirstNameLike(Long id , String firstName);
    List<StudentEntity> findAllByIdGreaterThanAndLastName(Long id, String lastName);

    List<StudentEntity> findAllByFirstNameAndLastName(String firstName,String lastName);
    List<StudentEntity> findAllByFirstName(String firstName);
    List<StudentEntity> findAllByLastName(String lastName);
    //jakarta persistence query language
    //JPQL
    //this query like findAll
    // s as like *
    @Query(value = "SELECT s FROM StudentEntity s WHERE s.firstName=?1 AND s.lastName=?2")
    List<StudentEntity> findByJpql(String firstName, String lastName);

//SQL
    //student_entity - this is table name from DB
//    @Query(value = "SELECT * FROM student_entity s where s.first_name=:firstName", nativeQuery = true)
//    List<StudentEntity> findBySql();


}
