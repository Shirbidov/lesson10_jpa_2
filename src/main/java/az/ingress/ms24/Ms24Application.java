package az.ingress.ms24;

import az.ingress.ms24.domain.StudentEntity;
import az.ingress.ms24.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms24Application implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static Specification<StudentEntity> idGraterThan(Long id) {
        return (root, query, criteriBuilder) ->
                criteriBuilder.greaterThan(root.get(StudentEntity.Fields.id), id);
    }


    public static void main(String[] args) {
        SpringApplication.run(Ms24Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        studentRepository.findAll(idGraterThan(3L))
                .stream().
                forEach(System.out::println);

        //	studentRepository.save(student1());
//	studentRepository.save(student2());
//
//    studentRepository.findAll()
//            .stream()
//            .forEach(System.out::println);

//        studentRepository.findDistinctByLastnameAndFirstname(1L)
//                .forEach(System.out::println);

//        studentRepository.findAllByIdGreaterThan(2L)
//                .stream()
//                .forEach(System.out::println);//
//
//                studentRepository.findAllByIdGreaterThanAndLastName(1L,"E%")
//                .stream()
//                .forEach(System.out::println);

//        studentRepository.findAll(Sort.by(Sort.Direction.DESC,"id"))
//                .stream()
//                .forEach(System.out::println);


//        final PageRequest pageRequest = PageRequest.of(0, 25, Sort.Direction.ASC, StudentEntity.Fields._id);
//
//        studentRepository.findAll(pageRequest)
//                .stream()
//                .forEach(System.out::println);

//        studentRepository.findByJpql("Elesker","Memmedov")
//                .stream()
//                .forEach(System.out::println);


//        studentRepository.findBySql()
//                .stream()
//                .forEach(System.out::println);


    }

    private StudentEntity student1() {
        return StudentEntity
                .builder()
                .firstName("Elesker")
                .lastName("Memmedov")
                .studentNumber("ING-4123213")
                .age(30)
                .build();
    }

    private StudentEntity student2() {
        return StudentEntity
                .builder()
                .firstName("Hesen")
                .lastName("Kamalli")
                .studentNumber("ING-3312111")
                .age(27)
                .build();
    }


}
